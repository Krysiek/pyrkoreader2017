// libs
/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
var saveAs=saveAs||function(e){"use strict";if(typeof e==="undefined"||typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return}var t=e.document,n=function(){return e.URL||e.webkitURL||e},r=t.createElementNS("http://www.w3.org/1999/xhtml","a"),o="download"in r,a=function(e){var t=new MouseEvent("click");e.dispatchEvent(t)},i=/constructor/i.test(e.HTMLElement)||e.safari,f=/CriOS\/[\d]+/.test(navigator.userAgent),u=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},s="application/octet-stream",d=1e3*40,c=function(e){var t=function(){if(typeof e==="string"){n().revokeObjectURL(e)}else{e.remove()}};setTimeout(t,d)},l=function(e,t,n){t=[].concat(t);var r=t.length;while(r--){var o=e["on"+t[r]];if(typeof o==="function"){try{o.call(e,n||e)}catch(a){u(a)}}}},p=function(e){if(/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)){return new Blob([String.fromCharCode(65279),e],{type:e.type})}return e},v=function(t,u,d){if(!d){t=p(t)}var v=this,w=t.type,m=w===s,y,h=function(){l(v,"writestart progress write writeend".split(" "))},S=function(){if((f||m&&i)&&e.FileReader){var r=new FileReader;r.onloadend=function(){var t=f?r.result:r.result.replace(/^data:[^;]*;/,"data:attachment/file;");var n=e.open(t,"_blank");if(!n)e.location.href=t;t=undefined;v.readyState=v.DONE;h()};r.readAsDataURL(t);v.readyState=v.INIT;return}if(!y){y=n().createObjectURL(t)}if(m){e.location.href=y}else{var o=e.open(y,"_blank");if(!o){e.location.href=y}}v.readyState=v.DONE;h();c(y)};v.readyState=v.INIT;if(o){y=n().createObjectURL(t);setTimeout(function(){r.href=y;r.download=u;a(r);h();c(y);v.readyState=v.DONE});return}S()},w=v.prototype,m=function(e,t,n){return new v(e,t||e.name||"download",n)};if(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob){return function(e,t,n){t=t||e.name||"download";if(!n){e=p(e)}return navigator.msSaveOrOpenBlob(e,t)}}w.abort=function(){};w.readyState=w.INIT=0;w.WRITING=1;w.DONE=2;w.error=w.onwritestart=w.onprogress=w.onwrite=w.onabort=w.onerror=w.onwriteend=null;return m}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content);if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs}else if(typeof define!=="undefined"&&define!==null&&define.amd!==null){define("FileSaver.js",function(){return saveAs})}

// "constants" hehe
var ITEM_NAME_CLS = '.programs__list-item';
var ITEM_TITLE_CLS = '.programs__program-title';
var ITEM_DATE_CLS = '.programs__program-date';
var ITEM_PLACE_CLS = '.programs__program-room';
var ITEM_DETAILS_CLS = '.programs__program-speakers';
var CSV_DELIMITER = '\t';
var HOUR_TITLE = 'Godz.';
var NON_STANDARD_HOUR = 'Inne';
var FRIDAY = 'Pt';
var SATURDAY = 'So';
var SUNDAY = 'Nd';
var ALL_DAYS = 'All';

// obj definintions
var Item = function(titleEl, dateEl, placeEl, detailsEl) {
	var me = this.constructor;
	
	this.title = me.parseTitle(titleEl);
	this.day = me.isAllTime(dateEl) ? me.parseDayFromTitle(titleEl) : me.parseDay(dateEl);
	this.time = me.isAllTime(dateEl) ? NON_STANDARD_HOUR : me.parseTime(dateEl);
	this.place = me.parsePlace(placeEl);
	this.details = me.parseDetails(detailsEl);
}

Item.parseTitle = function(el) {
	return el.text(); // as is
}

Item.parseDay = function(el) {
	var dateStr = el.text().substr(7); // usuniecie "Kiedy: "
	dateStr = dateStr.substring(0, dateStr.indexOf(',')); // od poczatku do pierwszego ,
	return dateStr;
}

Item.parseDayFromTitle = function(el) {
	var titleStr = el.text();
	if (titleStr.toLowerCase().indexOf('piątek') != -1) {
		return FRIDAY;
	}
	if (titleStr.toLowerCase().indexOf('sobot') != -1) {
		return SATURDAY;
	}
	if (titleStr.toLowerCase().indexOf('niedziel') != -1) {
		return SUNDAY;
	}
	return ALL_DAYS;
}

Item.parseTime = function(el) {
	var dateStr = el.text().substr(7); // usuniecie "Kiedy: "
	dateStr = dateStr.substring(dateStr.indexOf(',')+2); // od pierwszego , do konca
	dateStr = dateStr.substring(dateStr.indexOf(',')+2); // od drugiego , do konca
	return dateStr;
}

Item.parsePlace = function(el) {
	return el.text().substr(7); // usuniecie "Gdzie: "
}

Item.parseDetails = function(el) {
	return el.text(); // as is
}

Item.isAllTime = function(timeEl) {
	return timeEl.text().indexOf('Non-stop') != -1;
}

var TableBuilder = function(cols) {
	var content = [cols];
	var currLine = {};
	
	this.addRow = function(row) {
		if (row.length > cols.length) {
			console.log('Cos ci sie pojebalo zlotko');
			row = row.slice(0, cols.length);
		}
		content.push(row);
	}
	
	this.putCell = function(colName, cell) {
		if (cols.indexOf(colName) == -1) {
			console.log("Z czym mi tu kurwa wyjezdzasz, nie ma takiej kolumny: " + colName);
			return;
		}
		currLine[colName] = cell;
	}
	
	this.newLine = function() {
		var line = [];
		cols.forEach(function(col) {
			line.push(currLine[col] != null ? currLine[col] : "");
		});
		content.push(line);
		currLine = {};
	}
	
	this.parseCsv = function() {
		var str = "";
		content.forEach(function(line) {
			for (var i=0; i<cols.length; i++) {
				var cell = line[i] ? line[i] : '';
				str += cell + (i != cols.length-1 ? CSV_DELIMITER : '');
			};
			str += "\r\n";
		});
		return str;
	}
	
	this.parseHtml = function() {
		var str = "<table>";
		var firstLine = true;
		content.forEach(function(line) {
			str += "<tr>";
			if (firstLine) {
				for (var i=0; i<cols.length; i++) {
					str += "<th>";
					str += line[i];
					str += "</th>";
				};
				firstLine = false;
			} else {
				for (var i=0; i<cols.length; i++) {
					str += "<td>";
					var cell = line[i] ? line[i] : '';
					str += cell;
					str += "</td>";
				};
			}
			str += "</tr>";
		});
		str += "</table>";
		return str;
	}
}

// exec

var itemsEl = $(ITEM_NAME_CLS);
var items = [];

itemsEl.each(function(no, el) {
	items.push(
		new Item(
			$(el).find(ITEM_TITLE_CLS),
			$(el).find(ITEM_DATE_CLS),
			$(el).find(ITEM_PLACE_CLS),
			$(el).find(ITEM_DETAILS_CLS)
		)
	);
});

//var buffer = JSON.stringify(items, null, '\t');
//var blob = new Blob([buffer], {type: "text/plain;charset=utf-8"});
//saveAs(blob, "pyrkon2017.json");

var allDays = [];
items.forEach(function(item) {
	if (allDays.indexOf(item.day) == -1) {
		allDays.push(item.day);
	};
});

var itemsPerDay = {};
var placesPerDay = {};
var timesPerDay = {};
allDays.forEach(function(day) {
	var itemsFiltered = items.filter(function(item) {
		if (item.day == day) return true;
	});
	itemsPerDay[day] = itemsFiltered;
	
	var placesFiltered = [];
	itemsFiltered.forEach(function(item) {
		if (placesFiltered.indexOf(item.place) == -1) {
			placesFiltered.push(item.place);
		};
	});
	placesFiltered.sort(function(a, b) {
		if (a.toLowerCase() < b.toLowerCase()) return -1;
		if (a.toLowerCase() > b.toLowerCase()) return 1;
		return 0;
	});
	placesPerDay[day] = placesFiltered;

	var timesFiltered = [];
	itemsFiltered.forEach(function(item) {
		if (timesFiltered.indexOf(item.time) == -1) {
			timesFiltered.push(item.time);
		}
	});
	timesFiltered.sort(function(a, b) {
		var aSepIndex = a.indexOf(":");
		if (aSepIndex == -1) return 1;
		var aNum = Number(a.substr(0, aSepIndex)+a.substr(aSepIndex+1));
		var bSepIndex = b.indexOf(":");
		if (bSepIndex == -1) return -1;
		var bNum = Number(b.substr(0, bSepIndex)+b.substr(bSepIndex+1));
		return aNum - bNum;
	});
	timesPerDay[day] = timesFiltered;
});

// build csvs
allDays.forEach(function(day) {
	var csv = new TableBuilder([HOUR_TITLE].concat(placesPerDay[day]));
	
	timesPerDay[day].forEach(function(time) {
		csv.putCell(HOUR_TITLE, time);
		var itemsAtTime = itemsPerDay[day].filter(function(item) {
			if (item.time == time) {
				return true;
			}
		});
		itemsAtTime.forEach(function(item) {
			csv.putCell(item.place, item.title);
		});
		csv.newLine();
	});
	
	var blob = new Blob([csv.parseCsv()], {type: "text/plain;charset=utf-8"});
	//saveAs(blob, "pyrkon_prg_"+day+".csv");
});

// build htmls
allDays.forEach(function(day) {
	var htm = new TableBuilder([HOUR_TITLE].concat(placesPerDay[day]));
	
	timesPerDay[day].forEach(function(time) {
		htm.putCell(HOUR_TITLE, time);
		var itemsAtTime = itemsPerDay[day].filter(function(item) {
			if (item.time == time) {
				return true;
			}
		});
		itemsAtTime.forEach(function(item) {
			htm.putCell(item.place, "<b>" + item.title + "</b><br><em>" + item.details + "</em>");
		});
		htm.newLine();
	});
	
	var blob = new Blob([htm.parseHtml()], {type: "text/plain;charset=utf-8"});
	saveAs(blob, "pyrkon_prg_"+day+".html");
});
